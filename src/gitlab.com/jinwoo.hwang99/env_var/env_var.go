package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

// Request Handler
func handler(w http.ResponseWriter, r *http.Request) {
  // Loop begins outputting listof env_var of the process
	for _, pair := range os.Environ() {
		fmt.Fprintf(w, pair)
		fmt.Fprintf(w, "\n")
	}
	// end list
}

//
func main() {
  http.HandleFunc("/", handler)
  // listens on port 8080 and outputs listof env_var
	log.Printf("Listening on port 8080:")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
